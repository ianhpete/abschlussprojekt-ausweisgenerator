package de.micromata.ausweisgenerator;

import de.micromata.ausweisgenerator.entity.Visitor;
import de.micromata.ausweisgenerator.repository.VisitorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

@SpringBootApplication(exclude={DataSourceAutoConfiguration.class})
public class AusweisgeneratorApplication implements CommandLineRunner {

	@Autowired
	private VisitorRepository visitorRepository;

	public static void main(String[] args) {

		SpringApplication.run(AusweisgeneratorApplication.class, args);

	}

	public void run(String... args) throws Exception{

	}

}
