package de.micromata.ausweisgenerator.service;

import de.micromata.ausweisgenerator.entity.RenderCard;
import de.micromata.ausweisgenerator.repository.RenderRepository;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class RenderCardService {

    private RenderRepository renderRepository;

    public RenderCardService(RenderRepository renderRepository){
        this.renderRepository = renderRepository;
    }

    public RenderCard findRenderById(String id){
       return renderRepository.findById(id).get();
    }

    public List<RenderCard>getAll(){
        return renderRepository.findAll();
    }

    public void save(RenderCard card){
        renderRepository.save(card);
    }

}
