package de.micromata.ausweisgenerator.service;

import de.micromata.ausweisgenerator.entity.RenderCard;
import de.micromata.ausweisgenerator.entity.Templates;
import de.micromata.ausweisgenerator.entity.Visitor;
import de.micromata.ausweisgenerator.repository.GeneratorRepository;
import de.micromata.ausweisgenerator.repository.VisitorRepository;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.bson.BsonBinarySubType;
import org.bson.types.Binary;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;


@Service
public class GeneratorService {

    private VisitorService visitorService;
    private RenderCardService renderCardService;


    public GeneratorService(VisitorService visitorService, RenderCardService renderCardService){

        this.visitorService = visitorService;
        this.renderCardService = renderCardService;

    }

    public RenderCard generateCard(String visitorID, Templates template) {

        Configuration cfg = new Configuration();
        cfg.setDefaultEncoding("UTF-8");
        Writer stringWriter = new StringWriter();


        Visitor visitor = visitorService.findVisitor(visitorID);

        String templateHTML = new String(template.getTemplate().getData(), StandardCharsets.UTF_8);

        Map<String,String> input = new HashMap<String, String>();
        input.put("firstName",visitor.getFirstName());
        input.put("lastName", visitor.getLastName());
        input.put("company", visitor.getCompany());
        input.put("validity", visitor.getValidity());

        try {
            Template generatingTemplate = new Template(template.getTemplateName(),new StringReader(templateHTML),cfg);
            generatingTemplate.process(input,stringWriter);
        } catch (IOException | TemplateException e) {
            e.printStackTrace();
        }

        String cardHTML = stringWriter.toString();

        Binary pdfRender = new Binary(BsonBinarySubType.BINARY, pdfCardRender(cardHTML).getBytes());
        Binary webRender = new Binary(BsonBinarySubType.BINARY,webCardRender(cardHTML).getBytes());

        RenderCard card = new RenderCard(visitorID,visitor.getFirstName() + " " + visitor.getLastName(), webRender,pdfRender,null);

        renderCardService.save(card);

        return card;

    }

    public String webCardRender(String pdfRender){

        pdfRender = pdfRender.replace("1.5cm", "8rem");
        pdfRender = pdfRender.replace("3.7cm", "22rem");
        pdfRender = pdfRender.replace("9cm", "100%");
        pdfRender = pdfRender.replace("4em","6.5em");
        pdfRender = pdfRender.replace("0.7em","1.3em");
        pdfRender = pdfRender.replace("22em","27em");

        return pdfRender;

    }

    public String pdfCardRender(String pdfRender){
        pdfRender = pdfRender.replace("<h3>&nbsp;</h3>","");
        pdfRender = pdfRender.replace("22em","10em");
        pdfRender = pdfRender.replace("4.46cm","fit-content");

        return pdfRender;

    }

    public String binaryToString(Binary bin){
        return new String(bin.getData(), StandardCharsets.UTF_8);
    }

    public Binary stringToBinary(String string){
        return new Binary(BsonBinarySubType.BINARY, string.getBytes());
    }

}
