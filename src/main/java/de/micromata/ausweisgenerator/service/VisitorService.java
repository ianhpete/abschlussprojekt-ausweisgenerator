package de.micromata.ausweisgenerator.service;


import de.micromata.ausweisgenerator.entity.Visitor;
import de.micromata.ausweisgenerator.repository.VisitorRepository;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class VisitorService {

    private VisitorRepository visitorRepository;

    public VisitorService(VisitorRepository visitorRepository){

        this.visitorRepository = visitorRepository;

    }

    public List<Visitor> findAll(){
        return visitorRepository.findAll();
    }

    public void save(Visitor visitor){
        visitorRepository.save(new Visitor(visitor.getFirstName(),visitor.getLastName(), visitor.getCompany(), visitor.getValidity()));
    }

    public Visitor findVisitor(String id){
        return visitorRepository.findById(id).get();
    }

}
