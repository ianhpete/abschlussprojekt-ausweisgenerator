package de.micromata.ausweisgenerator.config;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import de.micromata.ausweisgenerator.entity.RenderCard;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.rest.core.config.RepositoryRestConfiguration;
import org.springframework.data.rest.webmvc.config.RepositoryRestConfigurerAdapter;

@Configuration
public class DBconfig {

    public @Bean MongoClient mongoClient(){
        return MongoClients.create("mongodb://localhost:27017");
    }
}
