package de.micromata.ausweisgenerator.controller;

import de.micromata.ausweisgenerator.entity.Visitor;
import de.micromata.ausweisgenerator.repository.VisitorRepository;
import de.micromata.ausweisgenerator.service.VisitorService;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import java.util.List;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class VisitorController {

    private VisitorService visitorService;

    public VisitorController(VisitorService visitorService){

        this.visitorService = visitorService;

    }

    @RequestMapping(method = RequestMethod.GET, path = "api/listvisitors")
    public ResponseEntity<?> listVisitors(){

        List<Visitor> response = visitorService.findAll();

        return ResponseEntity.ok(response);
    }

    @RequestMapping(method = RequestMethod.POST, path = "api/savevisitor")
    public ResponseEntity<?> saveVisitor(@RequestBody Visitor visitor){
        visitorService.save(visitor);

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setLocation(ServletUriComponentsBuilder
                .fromCurrentRequest().build().toUri());

        return new ResponseEntity<>(null, httpHeaders, HttpStatus.CREATED);
    }

}
