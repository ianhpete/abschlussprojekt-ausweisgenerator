package de.micromata.ausweisgenerator.controller;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.XMLWorkerHelper;
import com.openhtmltopdf.pdfboxout.DOMUtil;
import com.openhtmltopdf.pdfboxout.PdfRendererBuilder;
import de.micromata.ausweisgenerator.entity.GeneratorConf;
import de.micromata.ausweisgenerator.entity.RenderCard;
import de.micromata.ausweisgenerator.entity.Templates;
import de.micromata.ausweisgenerator.repository.GeneratorRepository;
import de.micromata.ausweisgenerator.service.GeneratorService;
import de.micromata.ausweisgenerator.service.RenderCardService;
import org.bson.BsonBinarySubType;
import org.bson.types.Binary;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;


import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.List;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class GeneratorController {

   private GeneratorService generatorService;
   private RenderCardService renderCardService;
   private GeneratorRepository generatorRepository;

   public GeneratorController(GeneratorRepository generatorRepository,RenderCardService renderCardService, GeneratorService generatorService){

       this.generatorRepository = generatorRepository;
       this.renderCardService = renderCardService;
       this.generatorService = generatorService;

   }

    @RequestMapping(method = RequestMethod.PATCH, path = "api/template/add")
    public ResponseEntity<?> addTemplate(@RequestPart("templateName") String templateName, @RequestPart("template") MultipartFile template) throws IOException{

        System.out.println(templateName);

        Binary b = new Binary(BsonBinarySubType.BINARY, template.getBytes());

        generatorRepository.save(new Templates(templateName,b));

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setLocation(ServletUriComponentsBuilder
                .fromCurrentRequest().build().toUri());

       return new ResponseEntity<>(null,httpHeaders, HttpStatus.CREATED);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(method = RequestMethod.GET, path = "api/template/get/{id}")
    public ResponseEntity<?> getTemplate(@PathVariable String id){
        System.out.println(id);

        Templates template =  generatorRepository.findById(id).get();
        String temp = generatorService.binaryToString(template.getTemplate());
        temp = generatorService.webCardRender(temp);
        Binary b = generatorService.stringToBinary(temp);

        return ResponseEntity.ok(b);
    }

    @RequestMapping(method = RequestMethod.GET, path = "api/listtemplates")
    public ResponseEntity<?> listTemplates(){

        List<Templates> response = generatorRepository.findAll();

        return ResponseEntity.ok(response);
    }

    @RequestMapping(method = RequestMethod.POST, path = "api/generate")
    public ResponseEntity<?> generateCard(@RequestBody GeneratorConf generatorConf){

        RenderCard generatedCard = generatorService.generateCard(generatorConf.getVisitorID(),generatorRepository.findById(generatorConf.getTemplateID()).get());

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setLocation(ServletUriComponentsBuilder
                .fromCurrentRequest().build().toUri());

        return new ResponseEntity<>(generatedCard, httpHeaders, HttpStatus.ACCEPTED);
    }

    @RequestMapping(method = RequestMethod.GET, path = "api/pdfdownload/{id}")
    public ResponseEntity<InputStreamResource> downloadPDF(@PathVariable String id){

       RenderCard card = renderCardService.findRenderById(id);

        try {
            OutputStream os = new FileOutputStream("templates/" + card.getCardOwner() + ".pdf");
            Document document = new Document();
            PdfWriter writer = PdfWriter.getInstance(document,os);
            document.open();
            document.setPageSize(new Rectangle(1,1));
            InputStream is = new ByteArrayInputStream(card.getPdfRender().getData());
            XMLWorkerHelper.getInstance().parseXHtml(writer, document,is);
            document.close();
            os.close();
        } catch (DocumentException | IOException e) {
            e.printStackTrace();
        }
        InputStreamResource response = null;
        try {
            response = new InputStreamResource(new FileInputStream("templates/" + card.getCardOwner() + ".pdf"));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setLocation(ServletUriComponentsBuilder
                .fromCurrentRequest().build().toUri());

        return  ResponseEntity.ok().headers(httpHeaders).contentType(MediaType.parseMediaType("application/pdf")).body(response);
    }


}
