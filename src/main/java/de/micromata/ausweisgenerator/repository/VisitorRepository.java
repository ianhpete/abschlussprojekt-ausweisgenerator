package de.micromata.ausweisgenerator.repository;

import de.micromata.ausweisgenerator.entity.Visitor;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface VisitorRepository extends MongoRepository<Visitor, String> {

}
