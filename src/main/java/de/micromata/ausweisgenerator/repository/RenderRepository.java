package de.micromata.ausweisgenerator.repository;

import de.micromata.ausweisgenerator.entity.RenderCard;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface RenderRepository extends MongoRepository<RenderCard,String> {
}
