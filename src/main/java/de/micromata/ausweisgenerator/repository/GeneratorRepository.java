package de.micromata.ausweisgenerator.repository;

import de.micromata.ausweisgenerator.entity.Templates;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface GeneratorRepository extends MongoRepository<Templates, String> {
}
