package de.micromata.ausweisgenerator.entity;

public class GeneratorConf {

    private String visitorID;
    private String templateID;

    public GeneratorConf(String visitorID, String templateID){
        this.visitorID = visitorID;
        this.templateID = templateID;
    }

    public String getTemplateID() {
        return templateID;
    }

    public String getVisitorID() {
        return visitorID;
    }
}
