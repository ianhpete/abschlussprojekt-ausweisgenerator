package de.micromata.ausweisgenerator.entity;

import org.bson.types.Binary;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "rendered")
public class RenderCard {

    @Id
    private String id;
    private String visitorID;
    private String cardOwner;
    private Binary webRender;
    private Binary pdfRender;
    private Binary printerRender;

    public RenderCard(String visitorID, String cardOwner, Binary webRender, Binary pdfRender, Binary printerRender){
        this.visitorID = visitorID;
        this.cardOwner = cardOwner;
        this.webRender = webRender;
        this.pdfRender = pdfRender;
        this.printerRender = printerRender;
    }

    public String getCardOwner() {
        return cardOwner;
    }

    public Binary getWebRender() {
        return webRender;
    }

    public Binary getPdfRender() {
        return pdfRender;
    }

    public Binary getPrinterRender() {
        return printerRender;
    }

    public String getVisitorID() {
        return visitorID;
    }

    public String getId() {
        return id;
    }
}
