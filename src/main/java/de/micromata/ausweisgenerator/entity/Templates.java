package de.micromata.ausweisgenerator.entity;

import org.bson.types.Binary;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "templates")
public class Templates {

    @Id
    private String id;
    private String templateName;
    private Binary template;


    public Templates(String templateName, Binary template){
        this.templateName = templateName;
        this.template = template;
    }

    public void setTemplate(Binary template) {
        this.template = template;
    }

    public void setTemplateName(String templateName) {
        this.templateName = templateName;
    }

    public Binary getTemplate() {
        return template;
    }

    public String getTemplateName() {
        return templateName;
    }

    public String getId() {
        return id;
    }
}
