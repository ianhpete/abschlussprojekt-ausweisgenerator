package de.micromata.ausweisgenerator.entity;


import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "visitors")
public class Visitor {

    @Id
    private String id;
    private String firstName;
    private String lastName;
    private String company;
    private String validity;

    public Visitor(String firstName, String lastName, String company, String validity){
        this.firstName = firstName;
        this.lastName = lastName;
        this.company = company;
        this.validity = validity;
    }

    public String getId(){
        return id;
    }

    public String getFirstName(){
        return firstName;
    }

    public String getLastName(){
        return lastName;
    }

    public String getCompany() {
        return company;
    }

    public String getValidity() {
        return validity;
    }

    @Override
    public String toString() {
        return "Visitor[id= " + id + ", Firstname= " + firstName + ", Lastname= " + lastName + ", Company= " + company + ", Validity= " + validity +  "]";
    }
}
