import Vue from 'vue'
import App from './App.vue'
import router from './router'
import VueFormulate from '@braid/vue-formulate'
import axios from 'axios'
import vSelect from 'vue-select'

Vue.component('v-select', vSelect)

Vue.use(VueFormulate)

Vue.prototype.$http = axios

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
